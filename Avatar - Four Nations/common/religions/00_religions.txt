# Religion-specific opinion can be added to modifiers (E.G., norse_pagan_opinion = 5), but the modifier *has* to be defined in 00_modifier_definitions.txt as well. It also needs localisation. Works for groups as well

# This trigger defines who can see a character's secret religion. The character with the secret religion is FROM, while the viewer is ROOT
# Characters can always see their own secret religion. The observer can see all secret religions
secret_religion_visibility_trigger = {
	secret_religion = FROM
}

earth_religions = {
	has_coa_on_barony_only = yes
	graphical_culture = indiangfx
	playable = yes
	hostile_within_group = yes
	color = { 0.8 0.4 0.0 }
	
	earth_spirituality = { # Spiritualist 

		icon = 2
		heresy_icon = 41

		color = { 28 135 12 }

		scripture_name = THE_LORE
		priest_title = PRIEST
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		#autocephaly = yes 
		god_names = {
			GOD_EARTH GOD_THE_EARTH GOD_THE_EARTH_SPIRIT GOD_THE_SPIRITS
		}
		high_god_name = GOD_THE_SPIRITS

		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 1
		cousin_marriage = no
		psc_marriage = no
		piety_name = SPIRITUALITY
		intermarry = air_acolytism
		intermarry = neo-nomadism
		intermarry = ba_sing_se_cultural_heritage
		intermarry = omashuan_ethics
		intermarry = path_of_jin_wei
		intermarry = path_of_wei_jin
		intermarry = the_great_serpent
		intermarry = the_divided
		intermarry = fortune_telling
		intermarry = way_of_chin
		intermarry = followers_of_kyoshi
		intermarry = piracy
		intermarry = the_great_swamp
	}
	ba_sing_se_cultural_heritage = { # Spiritualist 

		icon = 7
		heresy_icon = 41

		color = { 36 174 105 }

		scripture_name = THE_LORE
		priest_title = PRIEST
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		#autocephaly = yes 
		god_names = {
			GOD_EARTH GOD_THE_EARTH GOD_THE_EARTH_SPIRIT GOD_THE_SPIRITS
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 1
		cousin_marriage = no
		landed_kin_prestige_bonus = yes
		hard_to_convert = yes
		psc_marriage = no
		piety_name = SPIRITUALITY
		intermarry = air_acolytism
		intermarry = neo-nomadism
		intermarry = earth_spirituality
		intermarry = omashuan_ethics
		intermarry = path_of_jin_wei
		intermarry = path_of_wei_jin
		intermarry = the_great_serpent
		intermarry = the_divided
		intermarry = fortune_telling
		intermarry = way_of_chin
		intermarry = followers_of_kyoshi
		intermarry = piracy
	}
	omashuan_ethics = { # Spiritualist 

		icon = 19
		heresy_icon = 41

		color = { 131 135 12 }

		scripture_name = THE_LORE
		priest_title = PRIEST
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		#autocephaly = yes 
		god_names = {
			GOD_EARTH GOD_THE_EARTH GOD_THE_EARTH_SPIRIT GOD_THE_SPIRITS GOD_OMA GOD_SHU
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 1
		cousin_marriage = no
		psc_marriage = no
		piety_name = SPIRITUALITY
		intermarry = air_acolytism
		intermarry = neo-nomadism
		intermarry = earth_spirituality
		intermarry = ba_sing_se_cultural_heritage
		intermarry = path_of_jin_wei
		intermarry = path_of_wei_jin
		intermarry = the_great_serpent
		intermarry = the_divided
		intermarry = fortune_telling
		intermarry = way_of_chin
		intermarry = followers_of_kyoshi
		intermarry = piracy
		intermarry = the_great_swamp
	}
	path_of_jin_wei = { # Barbarism

		icon = 16
		heresy_icon = 41

		color = { 192 195 117 }

		scripture_name = THE_LORE
		priest_title = PRIEST
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		god_names = {
			GOD_EARTH GOD_THE_EARTH GOD_THE_EARTH_SPIRIT GOD_THE_SPIRITS GOD_JIN_WEI
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 1
		hard_to_convert = yes
		cousin_marriage = no
		attacking_same_religion_piety_loss = yes
		psc_marriage = no
		piety_name = CIVILITY
		intermarry = air_acolytism
		intermarry = neo-nomadism
		intermarry = earth_spirituality
		intermarry = ba_sing_se_cultural_heritage
		intermarry = omashuan_ethics
		intermarry = the_great_serpent
		intermarry = the_divided
		intermarry = fortune_telling
		intermarry = way_of_chin
		intermarry = followers_of_kyoshi
		intermarry = piracy
	}
	path_of_wei_jin = { # Barbarism

		icon = 18
		heresy_icon = 41

		color = { 61 63 16 }

		scripture_name = THE_LORE
		priest_title = PRIEST
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		god_names = {
			GOD_EARTH GOD_THE_EARTH GOD_THE_EARTH_SPIRIT GOD_THE_SPIRITS GOD_WEI_JIN
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 1
		hard_to_convert = yes
		cousin_marriage = no
		attacking_same_religion_piety_loss = yes
		psc_marriage = no
		piety_name = FREEDOM
		intermarry = air_acolytism
		intermarry = neo-nomadism
		intermarry = earth_spirituality
		intermarry = ba_sing_se_cultural_heritage
		intermarry = omashuan_ethics
		intermarry = the_great_serpent
		intermarry = the_divided
		intermarry = fortune_telling
		intermarry = way_of_chin
		intermarry = followers_of_kyoshi
		intermarry = piracy
	}
	the_great_serpent = { # Spiritualist 

		icon = 13
		heresy_icon = 41

		color = { 95 163 145 }

		scripture_name = THE_LORE
		priest_title = PRIEST
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		god_names = {
			GOD_EARTH GOD_THE_EARTH GOD_THE_EARTH_SPIRIT GOD_THE_SPIRITS GOD_THE_SERPENT
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS EVILGOD_THE_SERPENT
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 1
		cousin_marriage = no
		psc_marriage = no
		piety_name = SPIRITUALITY
		intermarry = air_acolytism
		intermarry = neo-nomadism
		intermarry = earth_spirituality
		intermarry = ba_sing_se_cultural_heritage
		intermarry = path_of_jin_wei
		intermarry = path_of_wei_jin
		intermarry = omashuan_ethics
		intermarry = the_divided
		intermarry = fortune_telling
		intermarry = way_of_chin
		intermarry = followers_of_kyoshi
		intermarry = piracy
	}
	the_divided = { # Spiritualist 

		icon = 50
		heresy_icon = 41

		color = { 174 91 50 }

		scripture_name = THE_LORE
		priest_title = PRIEST
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		god_names = {
			GOD_EARTH GOD_THE_EARTH GOD_THE_EARTH_SPIRIT GOD_THE_SPIRITS GOD_THE_GREAT_DEVIDE
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 1
		cousin_marriage = no
		psc_marriage = no
		piety_name = SPIRITUALITY
		intermarry = air_acolytism
		intermarry = neo-nomadism
		intermarry = earth_spirituality
		intermarry = ba_sing_se_cultural_heritage
		intermarry = path_of_jin_wei
		intermarry = path_of_wei_jin
		intermarry = omashuan_ethics
		intermarry = the_great_serpent
		intermarry = fortune_telling
		intermarry = way_of_chin
		intermarry = followers_of_kyoshi
		intermarry = piracy
	}
	fortune_telling = { # Spiritualist 

		icon = 14
		heresy_icon = 41

		color = { 255 215 0 }

		scripture_name = THE_DRAGON_BONES
		priest_title = FORTUNE_TELLER
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		god_names = {
			 GOD_THE_SPIRITS
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 1
		cousin_marriage = no
		psc_marriage = no
		piety_name = SPIRITUALITY
		intermarry = air_acolytism
		intermarry = neo-nomadism
		intermarry = earth_spirituality
		intermarry = ba_sing_se_cultural_heritage
		intermarry = path_of_jin_wei
		intermarry = path_of_wei_jin
		intermarry = omashuan_ethics
		intermarry = the_great_serpent
		intermarry = the_divided
		intermarry = way_of_chin
		intermarry = followers_of_kyoshi
		intermarry = piracy
	}
	unificationism = { # Imperialist

		icon = 37
		heresy_icon = 41

		color = { 137 165 143 }

		scripture_name = THE_LORE
		priest_title = PRIEST
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		god_names = {
			GOD_EARTH GOD_THE_EARTH GOD_THE_EARTH_SPIRIT GOD_THE_SPIRITS
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = yes
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 1
		cousin_marriage = no
		psc_marriage = no
		piety_name = SPIRITUALITY
		aggression = 1.5
		intermarry = outlaw_religions
		intermarry = air_acolytism
		intermarry = neo-nomadism
		intermarry = modern_religions
	}
	way_of_chin = { # Imperialist

		icon = 10
		heresy_icon = 41

		color = { 56 101 5 }

		scripture_name = THE_LORE
		priest_title = PRIEST
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		god_names = {
			GOD_EARTH GOD_THE_EARTH GOD_THE_EARTH_SPIRIT GOD_THE_SPIRITS
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = yes
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 1
		cousin_marriage = no
		landed_kin_prestige_bonus = yes
		psc_marriage = no
		piety_name = SPIRITUALITY
		aggression = 1.5
		intermarry = air_acolytism
		intermarry = neo-nomadism
		intermarry = earth_spirituality
		intermarry = ba_sing_se_cultural_heritage
		intermarry = path_of_jin_wei
		intermarry = path_of_wei_jin
		intermarry = omashuan_ethics
		intermarry = the_great_serpent
		intermarry = the_divided
		intermarry = fortune_telling
		intermarry = piracy
	}
	followers_of_kyoshi = { # Spiritualist 

		icon = 6
		heresy_icon = 41

		color = { 77 219 81 }

		scripture_name = THE_LORE
		priest_title = PRIEST
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		god_names = {
			GOD_EARTH GOD_THE_EARTH GOD_THE_EARTH_SPIRIT GOD_THE_SPIRITS
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}
		
		unit_modifier = {
			levy_size = 0.2
			retinuesize_perc = 0.3
			key = "OFF_RELIGION"
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 1
		cousin_marriage = no
		attacking_same_religion_piety_loss = yes
		psc_marriage = no
		piety_name = SPIRITUALITY
		aggression = 0.5
		intermarry = air_acolytism
		intermarry = neo-nomadism
		intermarry = earth_spirituality
		intermarry = ba_sing_se_cultural_heritage
		intermarry = omashuan_ethics
		intermarry = the_great_serpent
		intermarry = the_divided
		intermarry = fortune_telling
	}
	earth_monarchism = { # Imperialist

		icon = 7
		heresy_icon = 41

		color = { 23 64 25 }

		scripture_name = THE_LORE
		priest_title = PRIEST
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		god_names = {
			GOD_EARTH GOD_THE_EARTH GOD_THE_EARTH_SPIRIT GOD_THE_SPIRITS
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 1
		cousin_marriage = no
		landed_kin_prestige_bonus = yes
		psc_marriage = no
		piety_name = SPIRITUALITY
		intermarry = ba_sing_se_cultural_heritage
		intermarry = omashuan_ethics
		intermarry = path_of_jin_wei
		intermarry = path_of_wei_jin
		intermarry = the_great_serpent
		intermarry = the_divided
		intermarry = way_of_chin
	}
	children_of_si_wong = { # Spiritualist 

		icon = 48
		heresy_icon = 41

		color = { 220 233 38 }

		scripture_name = THE_LORE
		priest_title = PRIEST
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		defensive_attrition = yes
		allow_rivermovement = yes
		allow_looting = yes
		god_names = {
			GOD_EARTH GOD_THE_EARTH GOD_THE_EARTH_SPIRIT GOD_THE_SPIRITS
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 0
		cousin_marriage = no
		psc_marriage = no
		hard_to_convert = yes
		piety_name = SPIRITUALITY
	}
}

fire_religions = {
	has_coa_on_barony_only = yes
	graphical_culture = mesoamericangfx
	playable = yes
	hostile_within_group = yes
	color = { 0.8 0.4 0.0 }
	
	fire_spirituality = { # Spiritualist 

		icon = 3
		heresy_icon = 40

		color = { 193 12 12 }

		scripture_name = DRAGONBONE_SCROLLS
		has_heir_designation = yes
		priest_title = FIRE_SAGE
		priests_can_marry = yes # As Shyu's grandfather was Roku's mentor
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		#autocephaly = yes 
		short_reign_opinion_year_mult = 0.5
		god_names = {
			GOD_THE_SUN GOD_THE_FIRE_SPIRIT GOD_THE_SPIRITS GOD_THE_EMBER GOD_THE_PHOENIX
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}


		investiture = no
		can_have_antipopes = no
		can_excommunicate = yes
		can_call_crusade = no
		uses_decadence = no
		religious_clothing_head = 2
		religious_clothing_priest = 6
		cousin_marriage = no
		psc_marriage = no
		piety_name = SPIRITUALITY
		intermarry = air_acolytism
		intermarry = neo-nomadism
		intermarry = fire_nation_imperialism
		intermarry = bhanti_spirituality
	}
	
	fire_nation_imperialism = { # Imperialist
		
		icon = 9
		heresy_icon = 41

		color = { 105 12 12 }

		unit_modifier = {
			levy_size = 0.3
			
			retinuesize_perc = 0.5
			
			key = "OFF_RELIGION"
		}

		scripture_name = DRAGONBONE_SCROLLS
		aggression = 2.0
		has_heir_designation = yes
		priest_title = FIRE_SAGE
		priests_can_marry = yes # As Shyu's grandfather was Roku's mentor
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		seafarer = yes
		uses_decadence = no
		#autocephaly = yes 
		short_reign_opinion_year_mult = 0.5
		god_names = {
			GOD_THE_SUN GOD_THE_FIRE_SPIRIT GOD_THE_SPIRITS GOD_THE_EMBER GOD_THE_PHOENIX
		}
		high_god_name = GOD_THE_SUN
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}

		allow_looting = yes
		investiture = no
		can_have_antipopes = no
		can_excommunicate = yes
		can_call_crusade = no
		raised_vassal_opinion_loss = no
		peace_prestige_loss = yes
		religious_clothing_head = 2
		religious_clothing_priest = 2
		cousin_marriage = no
		landed_kin_prestige_bonus = yes
		psc_marriage = no
		piety_name = HONOUR
		intermarry = fire_spirituality
		intermarry = bhanti_spirituality
	}
		
	bhanti_spirituality = { # Pacifist
		icon = 36
		heresy_icon = 41
		pacifist = yes

		color = { 110 43 43 }

		scripture_name = DRAGONBONE_SCROLLS
		has_heir_designation = yes
		priest_title = FIRE_SAGE
		priests_can_marry = yes # As Shyu's grandfather was Roku's mentor
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		uses_decadence = no
		#autocephaly = yes 
		short_reign_opinion_year_mult = 0.5
		god_names = {
			GOD_THE_SUN GOD_THE_FIRE_SPIRIT GOD_THE_SPIRITS GOD_THE_EMBER GOD_THE_PHOENIX
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}


		investiture = no
		can_have_antipopes = no
		can_excommunicate = yes
		can_call_crusade = no
		religious_clothing_head = 2
		religious_clothing_priest = 6
		cousin_marriage = no
		hard_to_convert = yes
		attacking_same_religion_piety_loss = yes
		psc_marriage = no
		piety_name = SPIRITUALITY
		intermarry = fire_spirituality
		intermarry = fire_nation_imperialism
	}
	
	the_eternal_flame = { # Spiritualist 
		icon = 16
		heresy_icon = 41

		color = { 199 96 96 }
		
		has_heir_designation = yes
		priest_title = SUN_SAGE
		priests_can_marry = yes
		allow_rivermovement = yes
		defensive_attrition = yes
		uses_decadence = no
		short_reign_opinion_year_mult = 0.5
		god_names = {
			GOD_THE_DRAGONS GOD_THE_SUN
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = yes
		can_call_crusade = no
		religious_clothing_head = 3
		religious_clothing_priest = 6
		landed_kin_prestige_bonus = yes
		attacking_same_religion_piety_loss = yes
		piety_name = SPIRITUALITY
	}
}

air_religions = {
	has_coa_on_barony_only = yes
	graphical_culture = indiangfx
	playable = yes
	ai_convert_same_group = 0 #does not convert other religions
	ai_peaceful = yes 
	color = { 0.8 0.4 0.0 }
	
	nomadic_pacifism = { # Pacifist
		graphical_culture = westerngfx
		icon = 4
		pacifist = yes

		color = { 255 168 53 }
		
		character_modifier = {
			health = 0.1
			culture_flex = -0.5
			religion_flex = -0.5
			monthly_character_piety = 0.1
			global_revolt_risk = -0.01
			global_tax_modifier = 0.01
		}
		
		unit_home_modifier = {
			air_benders_defensive = 0.2
			air_benders_morale = 0.1
			key = "DEF_RELIGION"
		}

		investiture = yes
		has_heir_designation = yes
		can_have_antipopes = no
		can_retire_to_monastery = yes
		can_excommunicate = no
		scripture_name = SCROLL_OF_ELDERS
		priest_title = MONK
		female_temple_holders = yes
		can_grant_divorce = no # No marriage?
		can_call_crusade = no
		priests_can_marry = no
		priests_can_inherit = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		aggression = 0.0
		peace_piety_gain = 5.0 # Gains 5 piety each month while at peace

		god_names = {
			GOD_THE_SKIES GOD_THE_SKY GOD_YANGCHEN
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_LUST EVILGOD_GREED EVILGOD_MATERIALISM
		}

		religious_clothing_head = 1
		religious_clothing_priest = 4
		cousin_marriage = no
		attacking_same_religion_piety_loss = yes
		psc_marriage = no
		
		piety_name = SPIRITUALITY
		intermarry = nomadic_nihilism

	}
	nomadic_nihilism = { # Imperialist
		graphical_culture = westerngfx
		icon = 12

		color = { 255 216 164 }
		
		unit_modifier = {
			levy_size = 0.3
			retinuesize_perc = 0.5
			air_benders_offensive = 0.2
			key = "OFF_RELIGION"
		}

		investiture = yes
		has_heir_designation = yes
		can_have_antipopes = no
		can_retire_to_monastery = yes
		can_excommunicate = no
		scripture_name = WRITINGS_OF_LAGHIMA
		priest_title = MONK
		female_temple_holders = yes
		can_grant_divorce = no # No marriage?
		can_call_crusade = no
		priests_can_marry = no
		priests_can_inherit = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		aggression = 3.5
		peace_piety_gain = 5.0 # Gains 5 piety each month while at peace

		god_names = {
			GOD_THE_SKIES GOD_THE_SKY GOD_YANGCHEN
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_LUST EVILGOD_GREED EVILGOD_MATERIALISM
		}

		religious_clothing_head = 1
		religious_clothing_priest = 4
		cousin_marriage = no
		psc_marriage = no
		
		piety_name = SPIRITUALITY
		intermarry = nomadic_pacifism

	}
	air_acolytism = { # Pacifist
		graphical_culture = westerngfx
		icon = 4
		pacifist = yes

		color = { 182 116 29 }

		investiture = yes
		has_heir_designation = yes
		can_have_antipopes = no
		can_retire_to_monastery = yes
		can_excommunicate = no
		scripture_name = WRITINGS_OF_AVATAR_AANG
		priest_title = MONK
		female_temple_holders = yes
		can_grant_divorce = yes # No marriage?
		can_call_crusade = no
		priests_can_marry = yes
		priests_can_inherit = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		aggression = 0.0
		peace_piety_gain = 5.0 # Gains 5 piety each month while at peace

		god_names = {
			GOD_THE_SKIES GOD_THE_SKY GOD_YANGCHEN
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_LUST EVILGOD_GREED EVILGOD_MATERIALISM EVILGOD_SOZIN
		}

		religious_clothing_head = 0
		religious_clothing_priest = 4
		cousin_marriage = no
		attacking_same_religion_piety_loss = yes
		psc_marriage = no
		allow_in_ruler_designer = no
		piety_name = SPIRITUALITY
	}
	neo-nomadism = { # Spiritualist
		graphical_culture = westerngfx
		icon = 4
		pacifist = yes

		color = { 196 172 94 }

		investiture = yes
		has_heir_designation = yes
		can_have_antipopes = no
		can_retire_to_monastery = yes
		can_excommunicate = no
		scripture_name = WRITINGS_OF_AVATAR_AANG
		priest_title = MONK
		female_temple_holders = yes
		can_grant_divorce = yes # No marriage?
		can_call_crusade = no
		priests_can_marry = yes
		priests_can_inherit = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		aggression = 0.0
		peace_piety_gain = 5.0 # Gains 5 piety each month while at peace

		god_names = {
			GOD_THE_SKIES GOD_THE_SKY GOD_YANGCHEN
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_LUST EVILGOD_GREED EVILGOD_MATERIALISM EVILGOD_SOZIN
		}

		religious_clothing_head = 1
		religious_clothing_priest = 4
		cousin_marriage = no
		attacking_same_religion_piety_loss = yes
		psc_marriage = no
		intermarry = air_acolytism
		allow_in_ruler_designer = no
		piety_name = SPIRITUALITY

	}
}

water_religions = {
	has_coa_on_barony_only = yes
	graphical_culture = indiangfx
	playable = yes
	hostile_within_group = yes
	color = { 0.8 0.4 0.0 }
	
	southern_spirituality = { # Spiritualist

		icon = 1
		heresy_icon = 41

		color = { 42 35 236 }

		scripture_name = INUKSHUK_RUNES
		has_heir_designation = yes
		priest_title = WATER_SAGE
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		#autocephaly = yes 
		short_reign_opinion_year_mult = 0.5
		god_names = {
			GOD_THE_MOON GOD_THE_SPIRITS GOD_THE_OCEAN GOD_SPIRIT_OF_THE_OCEAN GOD_SPIRIT_OF_THE_MOON
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		religious_clothing_head = 5
		religious_clothing_priest = 5
		cousin_marriage = no
		psc_marriage = no
		piety_name = SPIRITUALITY
		intermarry = air_acolytism
		intermarry = neo-nomadism
		intermarry = tui_and_la
	}
	the_great_swamp = { # Spiritualist

		icon = 35
		heresy_icon = 41

		color = { 35 229 236 }

		has_heir_designation = yes
		priest_title = WATER_SAGE
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		defensive_attrition = yes
		#autocephaly = yes 
		short_reign_opinion_year_mult = 0.5
		god_names = {
			GOD_THE_MOON GOD_THE_SPIRITS GOD_THE_SWAMP
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		religious_clothing_head = 4
		religious_clothing_priest = 1
		cousin_marriage = no
		psc_marriage = no
		hard_to_convert = yes
		piety_name = SPIRITUALITY
		aggression = 0
		intermarry = air_acolytism
		intermarry = neo-nomadism
		intermarry = earth_spirituality
		intermarry = omashuan_ethics
	}
	tui_and_la = { # Spiritualist

		icon = 5
		heresy_icon = 41

		color = { 84 136 215 }

		scripture_name = INUKSHUK_RUNES
		has_heir_designation = yes
		priest_title = WATER_SAGE
		priests_can_marry = yes
		can_retire_to_monastery = yes
		female_temple_holders = no
		can_grant_divorce = yes 
		allow_rivermovement = yes
		matrilineal_marriages = no
		#autocephaly = yes 
		short_reign_opinion_year_mult = 0.5
		god_names = {
			GOD_THE_MOON GOD_THE_SPIRITS GOD_THE_OCEAN GOD_SPIRIT_OF_THE_OCEAN GOD_SPIRIT_OF_THE_MOON GOD_TUI_AND_LA
		}
		high_god_name = GOD_TUI_AND_LA
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS EVILGOD_THE_MOONSLAYER
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		religious_clothing_head = 5
		religious_clothing_priest = 5
		cousin_marriage = no
		psc_marriage = no
		piety_name = SPIRITUALITY
		intermarry = air_acolytism
		intermarry = neo-nomadism
		intermarry = southern_spirituality
	}
	witchcraft = { # Barbarism

		icon = 40
		heresy_icon = 41
		parent = southern_spirituality
		color = { 138 7 7 }

		priest_title = WITCH
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		dislike_tribal_organization = yes
		raised_vassal_opinion_loss = no
		#autocephaly = yes 
		god_names = {
			GOD_THE_DARK_SPIRITS
		}
		high_god_name = GOD_THE_DARK_SPIRITS
		evil_god_names = {
			EVILGOD_THE_SPIRITS
		}
		
		seafarer = yes
		allow_looting = yes
		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 5
		religious_clothing_priest = 5
		cousin_marriage = no
		psc_marriage = no
		piety_name = SPIRITUALITY
		allow_in_ruler_designer = yes
		aggression = 2
	}
	vaatuism = { # Dummied Out

		icon = 40
		heresy_icon = 41
		parent = tui_and_la
		color = { 92 2 171 }

		priest_title = PRIEST
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = no
		allow_rivermovement = yes
		raised_vassal_opinion_loss = no
		ignores_defensive_attrition = yes
		dislike_tribal_organization = yes
		god_names = {
			GOD_VAATU
		}
		high_god_name = GOD_VAATU
		evil_god_names = {
			EVILGOD_RAAVA
		}
		
		seafarer = yes
		allow_looting = yes
		investiture = no
		can_have_antipopes = no
		can_excommunicate = yes
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 5
		religious_clothing_priest = 5
		cousin_marriage = no
		psc_marriage = no
		piety_name = SPIRITUALITY
		aggression = 2
		allow_in_ruler_designer = no
	}
}

modern_religions = {
	has_coa_on_barony_only = yes
	graphical_culture = indiangfx
	playable = yes
	hostile_within_group = yes
	color = { 0.8 0.4 0.0 }
	
	modernism = { # Spiritualist

		icon = 40
		heresy_icon = 41

		color = { 196 196 37 }

		scripture_name = MODERN_PHILOSOPHY
		priest_title = PRIEST
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		#autocephaly = yes 
		god_names = {
			GOD_SPIRITS GOD_THE_SPIRITS
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 1
		cousin_marriage = no
		psc_marriage = no
		piety_name = SPIRITUALITY
		allow_in_ruler_designer = no
		intermarry = earth_religions
		intermarry = fire_religions
		intermarry = water_religions
		#intermarry = communist_religions
		#intermarry = lotus_religions
		intermarry = outlaw_religions
		intermarry = air_acolytism
		intermarry = neo-nomadism
	}
	
	equalism = { # Dummied Out

		icon = 40
		heresy_icon = 41

		color = { 144 109 123 }

		scripture_name = THE_EQUALIST_MANIFESTO
		priest_title = PRIEST
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = yes
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		god_names = {
			GOD_SPIRITS GOD_THE_SPIRITS
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS EVILGOD_THE_BENDERS
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = yes
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 1
		cousin_marriage = no
		psc_marriage = no
		piety_name = EQUALITY
		aggression = 2
		allow_in_ruler_designer = no
		intermarry = earth_religions
		intermarry = fire_religions
		intermarry = water_religions
		#intermarry = communist_religions
		#intermarry = lotus_religions
		intermarry = outlaw_religions
		intermarry = air_acolytism
		intermarry = neo-nomadism
	}
	
}

outlaw_religions = {
	has_coa_on_barony_only = yes
	graphical_culture = indiangfx
	playable = yes
	hostile_within_group = yes
	color = { 0.8 0.4 0.0 }
	
	anarchism = { # Dummied Out

		icon = 40
		heresy_icon = 41

		color = { 226 133 133 }

		priest_title = PRIEST
		priests_can_marry = yes
		feminist = yes # Nullifies the negative opinion modifier that vassals normally get if ruler is female or has female heir
		can_retire_to_monastery = no
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		#autocephaly = yes 
		god_names = {
			GOD_SPIRITS GOD_THE_SPIRITS GOD_FREEDOM
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS EVILGOD_GOVERNMENT
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 3
		cousin_marriage = no
		psc_marriage = no
		max_consorts = 3
		piety_name = FREEDOM
		aggression = 2
		allow_in_ruler_designer = no
	}
	piracy = { # Barbarism

		icon = 11
		heresy_icon = 41

		color = { 60 61 60 }

		priest_title = PRIEST
		priests_can_marry = yes
		can_retire_to_monastery = no
		female_temple_holders = yes
		can_grant_divorce = yes 
		allow_rivermovement = yes
		allow_looting = yes
		#autocephaly = yes
		seafarer = yes
		god_names = {
			GOD_SPIRITS GOD_THE_SPIRITS GOD_FREEDOM GOD_BOOTY GOD_MERMAIDS
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS EVILGOD_GOVERNMENT EVILGOD_THE_KRAKEN
		}

		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 3
		cousin_marriage = no
		psc_marriage = no
		max_consorts = 3
		piety_name = FREEDOM
		aggression = 2
		intermarry = ba_sing_se_cultural_heritage
		intermarry = omashuan_ethics
		intermarry = path_of_jin_wei
		intermarry = path_of_wei_jin
		intermarry = the_great_serpent
		intermarry = the_divided
		intermarry = fortune_telling
		intermarry = way_of_chin
		intermarry = earth_spirituality
	}
	barbarism = { # Barbarism

		icon = 20
		heresy_icon = 20

		color = { 155 23 58 }

		priest_title = PRIEST
		priests_can_marry = yes
		can_retire_to_monastery = no
		female_temple_holders = no
		can_grant_divorce = yes 
		allow_rivermovement = yes
		allow_looting = yes
		#autocephaly = yes
		seafarer = yes
		god_names = {
			GOD_SPIRITS GOD_THE_SPIRITS GOD_FREEDOM
		}
		high_god_name = GOD_THE_SPIRITS
		evil_god_names = {
			EVILGOD_THE_DARK_SPIRITS
		}
		max_consorts = 1
		investiture = no
		can_have_antipopes = no
		can_excommunicate = no
		can_call_crusade = no
		short_reign_opinion_year_mult = 1
		religious_clothing_head = 4
		religious_clothing_priest = 3
		cousin_marriage = no
		psc_marriage = no
		piety_name = SPIRITUALITY
		aggression = 3
	}
	
}
